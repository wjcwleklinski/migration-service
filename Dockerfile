FROM eclipse-temurin:21-alpine
ENV APP_HOME=/usr/local/migration-service-app
ENV JAR_FILE=build/libs/migration-service-*.jar
ENV JAVA_TEMPDIR=${APP_HOME}/tmp
ENV JAVA_ARGS=""
ENV LIST_SERVICE_URL=localhost \
    LIST_SERVICE_USERNAME=list \
    LIST_SERVICE_PASSWORD=list \
    POSTGRES_URL=localhost \
    POSTGRES_USERNAME=postgres \
    POSTGRES_PASSWORD=postgres \
    KEYCLOAK_USERNAME=keycloak \
    KEYCLOAK_PASSWORD=keycloak

COPY ${JAR_FILE} ${APP_HOME}/app.jar

USER root
RUN chown -R 1001:0 ${APP_HOME} && mkdir ${JAVA_TEMPDIR} && chmod -R ug+rwX ${APP_HOME}
USER 1001

CMD cd ${APP_HOME} && java -Djava.io.tmpdir=${JAVA_TEMPDIR} ${JAVA_ARGS} -jar -Dmigration.service.list-service.url=${LIST_SERVICE_URL} -Dmigration.service.list-service.username=${LIST_SERVICE_USERNAME} -Dmigration.service.list-service.password=${LIST_SERVICE_PASSWORD} -Dmigration.service.postgres.url=${POSTGRES_URL} -Dmigration.service.postgres.username=${POSTGRES_USERNAME} -Dmigration.service.postgres.password=${POSTGRES_PASSWORD} -Dmigration.keycloak.username=${KEYCLOAK_USERNAME} -Dmigration.keycloak.password=${KEYCLOAK_PASSWORD} app.jar