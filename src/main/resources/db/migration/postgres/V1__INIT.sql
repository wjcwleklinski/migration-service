create user list with password '${list-password}';
create database list with owner list;

create user keycloak with password '${keycloak-password}';
create database keycloak with owner keycloak;