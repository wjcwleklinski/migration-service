DO $$
BEGIN
    IF NOT COLUMN_EXISTS('ETR_ENTRIES', 'ETR_IS_SELECTED') THEN
        ALTER TABLE ETR_ENTRIES ADD COLUMN ETR_IS_SELECTED BOOLEAN;
    END IF;
END $$;
