package com.wjcwleklinski.migrationservice.migration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "flyway")
public class FlywayPlaceholders {

    private Map<String, String> placeholders;
}
