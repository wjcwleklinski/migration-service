package com.wjcwleklinski.migrationservice.migration;

public interface FlywayDataSource {

    String getUrl();
    String getUsername();
    String getPassword();
    String getLocation();
    String getSchema();
    Integer getOrder();

}
