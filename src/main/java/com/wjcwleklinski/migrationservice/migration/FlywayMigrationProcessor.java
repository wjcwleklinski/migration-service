package com.wjcwleklinski.migrationservice.migration;

import org.flywaydb.core.Flyway;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;

@Component
public class FlywayMigrationProcessor {

    public FlywayMigrationProcessor(FlywayPlaceholders placeholders, List<FlywayDataSource> dataSources) {
        processMigration(placeholders, dataSources);
    }

    private void processMigration(FlywayPlaceholders placeholders, List<FlywayDataSource> dataSources) {
        dataSources.sort(Comparator.comparing(FlywayDataSource::getOrder));
        for (FlywayDataSource dataSource : dataSources) {
            Flyway flyway = Flyway.configure()
                    .locations(dataSource.getLocation())
                    .mixed(true)
                    .baselineOnMigrate(true)
                    .placeholders(placeholders.getPlaceholders())
                    .dataSource(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword())
                    .schemas(dataSource.getSchema())
                    .load();
            flyway.migrate();
        }
    }
}
