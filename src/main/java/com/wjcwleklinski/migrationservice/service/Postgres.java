package com.wjcwleklinski.migrationservice.service;

import com.wjcwleklinski.migrationservice.migration.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "migration.service.postgres")
public class Postgres extends Service implements FlywayDataSource {

}
