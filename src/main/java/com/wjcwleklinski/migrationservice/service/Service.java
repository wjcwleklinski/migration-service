package com.wjcwleklinski.migrationservice.service;

import lombok.Data;

@Data
public abstract class Service {

    private String url;
    private String username;
    private String password;
    private String location;
    private String schema;
    private Integer order;
}
