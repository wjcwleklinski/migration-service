package com.wjcwleklinski.migrationservice.service;

import com.wjcwleklinski.migrationservice.migration.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "migration.service.list-service")
public class ListService extends Service implements FlywayDataSource {

}
